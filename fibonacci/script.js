'use strict';
function fibonacci(nthNumber = 0) {
  const sequence = [1, 1];
  let index = 2;
  while (index < nthNumber) {
    sequence.push(sequence[0] + sequence[1]);
    sequence.shift();
    index++;
  }
  return (nthNumber >= 1) ? sequence.pop() : 'Nth number must be greater than or equal to 1';
}

console.log(fibonacci(0));  // => 'Nth number must be greater than or equal to 1'
console.log(fibonacci(1));  // => 1
console.log(fibonacci(2));  // => 1
console.log(fibonacci(3));  // => 2
console.log(fibonacci(4));  // => 3
console.log(fibonacci(5));  // => 5
console.log(fibonacci(10)); // => 55
console.log(fibonacci(20)); // => 6765