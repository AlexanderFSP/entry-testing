'use strict';

const gulp         = require('gulp'),
      browserSync  = require('browser-sync').create(),
      reload       = browserSync.reload;

const path = {
  brokenPage: './broken-page',
  maze: './maze',
  pageFromJpeg: './page-from-jpeg',
};

/**
 *  Запуск сервера...
 */
gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: path.maze
    },
    notify: false
  });
});

/**
 *  Наблюдение за html & обновление...
 */
gulp.task('watch', function() {
  gulp.watch(path.maze + '/*.html').on('change', reload);
});

gulp.task('default', gulp.parallel('browserSync', 'watch'));